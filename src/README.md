# ROS作业
本项目主要完成小鼠走迷宫，灵感来源于竞赛micro mouse
## 设计目的
 希望本项目中能包含更多老师所讲的知识。
## 设计简介
|    Name   |  Function  | Description |
|    ---    |  ---    |    ---         |
|prim_maze  |地图创建  | 应用prim算法生成随即迷宫
|mice_maze  |迷宫行进  | 控制小鼠运动的包
|rviz_maze  |可视化包  | 把小鼠在迷宫中的建图展示出来