from launch import LaunchDescription
from launch_ros.actions import Node
from launch.actions import DeclareLaunchArgument
from launch.substitutions import LaunchConfiguration

def generate_launch_description():
    ld = LaunchDescription()
    #像机选取
    param = DeclareLaunchArgument(
        'complexity',
        default_value='19.0',
        description='Parameter 1'
    )
    ld.add_action(param)
    rviz= Node(
        package='rviz2',
        executable='rviz2',
    )
    ld.add_action(rviz)
    prim = Node(
        package='prim_maze',
        executable='prim_maze_node',
        parameters=[
            # 从launch参数中获取参数值
            {'complexity': LaunchConfiguration('complexity')},
        ]
        )
    ld.add_action(prim)
    #其余节点
    mice = Node(
        package='mice_maze',
        executable='mice_maze_node',
        parameters=[
            # 从launch参数中获取参数值
            {'complexity': LaunchConfiguration('complexity')},
        ]
    )
    ld.add_action(mice)
    return ld