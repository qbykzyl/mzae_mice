#include <iostream>
#include <rclcpp/rclcpp.hpp>
#include <rclcpp/logging.hpp>
#include <opencv2/opencv.hpp>
#include <sensor_msgs/msg/image.hpp>
#include <rclcpp/publisher.hpp>
#include <stack>
#include <cv_bridge/cv_bridge.h>

// #include <rclcpp/rclcpp.hpp>

using namespace std;
using namespace cv;

namespace maze
{
    class PrimMazeNode : public rclcpp::Node
    {
        rclcpp::Publisher<sensor_msgs::msg::Image>::SharedPtr img_pub_;
        sensor_msgs::msg::Image image_msg_;
        std::map<std::string, double> dic_; // 调控相关
    public:
        PrimMazeNode(const rclcpp::NodeOptions &options = rclcpp::NodeOptions());
        ~PrimMazeNode();
        void generateMaze(Mat &maze);
        void initParam();

    };
}