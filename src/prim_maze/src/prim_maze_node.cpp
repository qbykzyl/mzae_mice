#include "../include/prim_maze_node.hpp"
namespace maze
{

    void PrimMazeNode::generateMaze(Mat &maze)
    {
        srand(static_cast<unsigned>(time(nullptr)));

        // 定义方向，上、下、左、右
        const int dir_row[] = {-1, 1, 0, 0};
        const int dir_col[] = {0, 0, -1, 1};

        std::stack<std::pair<int, int>> stk;
        stk.push(std::make_pair(0, 0)); // 从起点开始生成

        while (!stk.empty() && rclcpp::ok())
        {
            int r = stk.top().first;
            int c = stk.top().second;
            stk.pop();

            maze.at<Vec3b>(r, c) = cv::Vec3b(255, 255, 255); // 设置当前位置为通路

            // 随机打乱四个方向
            std::vector<int> directions = {0, 1, 2, 3};
            std::random_shuffle(directions.begin(), directions.end());

            for (int i = 0; i < 4; ++i)
            {
                int new_r = r + dir_row[directions[i]] * 2; // 下一步位置
                int new_c = c + dir_col[directions[i]] * 2;

                if (new_r >= 0 && new_r <= dic_["complexity"] && new_c >= 0 && new_c <= dic_["complexity"] && maze.at<Vec3b>(new_r, new_c) == cv::Vec3b(0, 0, 0))
                {
                    maze.at<Vec3b>(new_r, new_c) = cv::Vec3b(255, 255, 255);                                           // 设置新位置为通路
                    maze.at<Vec3b>(r + dir_row[directions[i]], c + dir_col[directions[i]]) = cv::Vec3b(255, 255, 255); // 设置墙壁间的通路
                    stk.push(std::make_pair(new_r, new_c));
                }
            }
            cv::imshow("maze", maze);
            cv::waitKey(5);
        }
        // maze.at<Vec3b>(0, 0) = cv::Vec3b(0, 0, 255);
        // maze.at<Vec3b>(dic_["complexity"] - 1, dic_["complexity"] - 1) = cv::Vec3b(255, 0, 0);
        return;
    }
    PrimMazeNode::PrimMazeNode(const rclcpp::NodeOptions &options) : Node("prim_maze_node", options)
    {
        initParam();
        RCLCPP_INFO(this->get_logger(), "Starting prim_maze node...");
        img_pub_ = this->create_publisher<sensor_msgs::msg::Image>("image_raw", rclcpp::SensorDataQoS());
        Mat maze(dic_["complexity"], dic_["complexity"], CV_8UC3, Scalar(0, 0, 0));

        cv::namedWindow("maze", cv::WINDOW_NORMAL);
        // 初始化迷宫

        RCLCPP_INFO(this->get_logger(), "Starting map");
        // 使用Prim算法生成迷宫
        generateMaze(maze);

        // 显示迷宫

        while (rclcpp::ok())
        {
            // generateMaze(maze);
            RCLCPP_INFO(this->get_logger(), "Finish map");
            image_msg_.header.frame_id = "left";
            image_msg_.encoding = "rgb8";
            cv::imshow("maze", maze);
            if (cv::waitKey(5) == ' ')
            {
                maze = Mat(dic_["complexity"], dic_["complexity"], CV_8UC3, Scalar(0, 0, 0));
                generateMaze(maze);
            }
            image_msg_.header.stamp = this->get_clock()->now();
            image_msg_.data.assign(maze.datastart, maze.dataend);
            image_msg_.height = maze.rows;
            image_msg_.width = maze.cols;
            image_msg_.step = maze.cols * 3;
            image_msg_.data.resize(maze.rows * maze.cols * 3);
            img_pub_->publish(std::move(image_msg_));
        }
        return;
    }
    void PrimMazeNode::initParam()
    {
        this->declare_parameter("complexity", 19.0);
        this->get_parameter("complexity", dic_["complexity"]);
        dic_["complexity"] = (dic_["complexity"] * 2 + 1);
    }
    PrimMazeNode::~PrimMazeNode()
    {
        cv::destroyAllWindows();
    }
}

int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<maze::PrimMazeNode>());
    rclcpp::shutdown();
    return 0;
}
#include "rclcpp_components/register_node_macro.hpp"
// Register the component with class_loader.
// This acts as a sort of entry point, allowing the component to be discoverable when its library
// is being loaded into a running process.
RCLCPP_COMPONENTS_REGISTER_NODE(maze::PrimMazeNode)
