
#include <rclcpp/publisher.hpp>
#include <rclcpp/rclcpp.hpp>
#include <sensor_msgs/msg/camera_info.hpp>
#include <sensor_msgs/msg/image.hpp>
#include <sensor_msgs/msg/camera_info.hpp>

#include <visualization_msgs/msg/marker_array.hpp>
#include <rclcpp/duration.hpp>
#include <rclcpp/qos.hpp>
#include <tf2_geometry_msgs/tf2_geometry_msgs.hpp>
#include <cv_bridge/cv_bridge.h>
#include <rmw/qos_profiles.h>
#include <tf2/LinearMath/Matrix3x3.h>
#include <tf2/convert.h>
#include <ament_index_cpp/get_package_share_directory.hpp>
#include <image_transport/image_transport.hpp>
#include <image_transport/publisher.hpp>
#include <image_transport/subscriber_filter.hpp>
// STD
#include <memory>
#include <string>
#include <vector>
#include <Eigen/Dense>
#include <Eigen/Core>
#include <opencv2/opencv.hpp>
// #include <opencv2/core/eigen.hpp>
#include <algorithm>
#include <map>
#include <chrono>
// #include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>

using namespace std;
using namespace cv;
namespace maze
{
    class MiceMazeNode : public rclcpp::Node
    {
    public:
        rclcpp::Subscription<sensor_msgs::msg::Image>::SharedPtr img_sub_;
        rclcpp::Publisher<visualization_msgs::msg::MarkerArray>::SharedPtr marker_array_pub_; // rviz消息
        visualization_msgs::msg::MarkerArray markers_msg_; // 可视化消息
        std::map<std::string, double> dic_; // 调控相关
        cv::Point mice_=cv::Point(0,0);
        std::vector<Point> shortest_path_;
        int cell_=0;
        std::vector<Point> directions = {Point(1, 0), Point(0, 1), Point(0, -1), Point(-1, 0)}; // 定义四个方向
        explicit MiceMazeNode(const rclcpp::NodeOptions &options = rclcpp::NodeOptions());
        ~MiceMazeNode();
        void imageCallback( sensor_msgs::msg::Image::ConstSharedPtr img_msg);
        void showImage(cv::Mat src);
        bool miceServer(cv::Mat &src,Point current,std::vector<Point>current_path_);
        void processorMarker(Point wall,int mode);
        void initParam();
        // bool dfs(Point current);

    };
}