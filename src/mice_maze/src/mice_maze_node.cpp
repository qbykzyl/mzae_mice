#include "../include/mice_maze_node.hpp"

using namespace std;
using namespace cv;
namespace maze
{
    MiceMazeNode::MiceMazeNode(const rclcpp::NodeOptions &options) : Node("mice_maze_node", options)
    {
        RCLCPP_INFO(this->get_logger(), "Starting mice_maze node...");
        img_sub_ = this->create_subscription<sensor_msgs::msg::Image>(
            "/image_raw",
            rclcpp::SensorDataQoS(),
            std::bind(&MiceMazeNode::imageCallback,
                      this, std::placeholders::_1));
        initParam();
        marker_array_pub_ = this->create_publisher<visualization_msgs::msg::MarkerArray>("wall_marker_msg", 10);
    }

    MiceMazeNode::~MiceMazeNode()
    {
    }
    void MiceMazeNode::processorMarker(Point wall, int mode)
    {
        visualization_msgs::msg::Marker wall_marker;
        wall_marker.header.stamp = this->get_clock()->now();
        wall_marker.header.frame_id = "bace_link";                 // 设置时间戳
        wall_marker.ns = "markers";                                // 设置命名空间
        wall_marker.id = cell_++;                                  // 设置唯一标识符
        wall_marker.type = visualization_msgs::msg::Marker::CUBE;  // 设置类型为立方体
        wall_marker.action = visualization_msgs::msg::Marker::ADD; // 添加新的立方体
        if (mode)
        {
            geometry_msgs::msg::Vector3 scale;
            scale.x = 1; // 设置立方体的大小
            scale.y = 1;
            scale.z = 2;
            wall_marker.scale = scale;
            std_msgs::msg::ColorRGBA color;
            color.r = 1; // 设置立方体的颜色为红色
            color.g = 0;
            color.b = 1;
            color.a = 1.0;
            wall_marker.color = color;
        }
        else
        {
            geometry_msgs::msg::Vector3 scale;
            scale.x = 1; // 设置立方体的大小
            scale.y = 1;
            scale.z = 0.5;
            wall_marker.scale = scale;
            std_msgs::msg::ColorRGBA color;
            color.r = 0.0; // 设置立方体的颜色为红色
            color.g = 0.71;
            color.b = 0.0;
            color.a = 1.0;
            wall_marker.color = color;
        }
        wall_marker.pose.position.x = wall.x - dic_["complexity"] / 2; // 设置箭头的位置 x 坐标
        wall_marker.pose.position.y = wall.y - dic_["complexity"] / 2; // 设置箭头的位置 y 坐标
        wall_marker.pose.position.z = 0;
        wall_marker.pose.orientation.x = 0.0; // 设置箭头的方向
        wall_marker.pose.orientation.y = 0.0;
        wall_marker.pose.orientation.z = 0.0;
        wall_marker.pose.orientation.w = 1.0;
        markers_msg_.markers.emplace_back(wall_marker);
    }
    bool MiceMazeNode::miceServer(cv::Mat &src, Point current, std::vector<Point> current_path)
    {

        if (current == Point(src.cols - 1, src.rows - 1))
        {
            if (shortest_path_.empty() || current_path.size() < shortest_path_.size())
            {
                shortest_path_ = current_path;
            }
            // 找到出口
            return true;
        }
        for (const Point &dir : directions)
        {
            Point next = current + dir;
            if (next.x < 0 || next.x >= src.cols || next.y < 0 || next.y >= src.rows || (src.at<Vec3b>(next) == cv::Vec3b(0, 0, 0)))
            {
                processorMarker(next, 1);
                marker_array_pub_->publish(markers_msg_);
            }
        }
        // 尝试每一个方向
        for (const Point &dir : directions)
        {
            Point next = current + dir;
            if (next.x >= 0 && next.x < src.cols && next.y >= 0 && next.y < src.rows && src.at<Vec3b>(next) == cv::Vec3b(255, 255, 255))
            {
                cv::Mat dst = src.clone();
                dst.at<Vec3b>(current) = Vec3b(0, 255, 0); // 标记为已访问
                std::vector<Point> next_path = current_path;
                next_path.push_back(next);
                showImage(dst);
                if (miceServer(dst, next, next_path))
                {
                    src = dst;
                }
            }
        }

        return false;
    }

    void MiceMazeNode::imageCallback(sensor_msgs::msg::Image::ConstSharedPtr img_msg)
    {
        if (!img_msg)
        {
            RCLCPP_ERROR_THROTTLE(this->get_logger(), *this->get_clock(), 500, "\033[1m\033[41m[msg error\033[0m");
            return;
        }
        // RCLCPP_INFO(this->get_logger(), "mice enter the maze");
        cv::Mat src = cv_bridge::toCvShare(img_msg, "rgb8")->image; // 图像消息转为Mat矩阵
        cv::resize(src, src, cv::Size(dic_["complexity"], dic_["complexity"]));
        srand(static_cast<unsigned>(time(nullptr)));
        std::random_shuffle(directions.begin(), directions.end());
        std::vector<Point> current_path;
        if (miceServer(src, mice_, current_path))
        {
            RCLCPP_INFO(this->get_logger(), "Slove");
        }
        else
        {
            RCLCPP_INFO(this->get_logger(), "fail");
        }
        for (const auto &point : shortest_path_)
        {
            RCLCPP_INFO(this->get_logger(), "Point: (%d, %d)", point.x, point.y);
            // 标记最短路径
            processorMarker(point, 0);
            marker_array_pub_->publish(markers_msg_);
            src.at<Vec3b>(point) = Vec3b(0, 0, 255);
            showImage(src);
        }
        showImage(src);
        // cell_=0;
        // markers_msg_.markers.empty();
    }
    void MiceMazeNode::showImage(cv::Mat src)
    {
        cv::namedWindow("map", cv::WINDOW_NORMAL);
        cv::imshow("map", src);
        cv::waitKey(5);
    }
    void MiceMazeNode::initParam()
    {
        this->declare_parameter("complexity", 19.0);
        this->get_parameter("complexity", dic_["complexity"]);
        dic_["complexity"] = (dic_["complexity"] * 2 + 1);
    }
}
int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);
    rclcpp::NodeOptions options;
    rclcpp::spin(std::make_shared<maze::MiceMazeNode>((options)));
    rclcpp::shutdown();
    return 0;
}
#include "rclcpp_components/register_node_macro.hpp"
// Register the component with class_loader.
// This acts as a sort of entry point, allowing the component to be discoverable when its library
// is being loaded into a running process.
RCLCPP_COMPONENTS_REGISTER_NODE(maze::MiceMazeNode)